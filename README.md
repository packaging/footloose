# [Ignite](https://github.com/weaveworks/footloose) apt packages

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-footloose.asc https://packaging.gitlab.io/footloose/gpg.key
```

### Add repo to apt

```bash
echo "deb [arch=amd64] https://packaging.gitlab.io/footloose footloose main" | sudo tee /etc/apt/sources.list.d/morph027-footloose.list
```
